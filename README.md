# Java Backend
Java backend is a tiny application, which was requested to be coded. It handles very simple operations of currencies - adding or removing.
App is storing values only per run. After closing app, all values gathered are gone. 

# Requirements
  - Java 8 installed
  - Maven

# Running the app

 - You can install the app using mvn ehich downloads all the modules needed:
```sh
$ mvn install
```

 - You can start app by running your cmd interpreter using:
```sh
$ mvn exec:java -Dexec.mainClass=sk.peefg.javabackend.Main -Dexec.args="files/file.txt"
# or use whichever path you'd like

# or do not attach any file, if you don't want to
$ mvn exec:java -Dexec.mainClass=sk.peefg.javabackend.Main
```

 - You can run tests using:
```sh
$ mvn tests
```

 - You can generate javadoc for yourself with cmd:
```sh
$ mvn generate-sources javadoc:javadoc
```

# App commands
`CURR AMOUNT` -> `USD 35` or `RMB -75.69` (adds or removes amount for specified value)
`quit` (quits the program)


# Validations
program checks:
- for invalid input (valid input is `CURR AMOUNT` , divided by comma)
- for typed currency - if it has been implemented
- if value (amount of money) is regular number
- if negative input (withdrawal) is able to be completed (partial withdrawal is not allowed)
  
# Behaviour
- Program outputs gathered data to the system console each 60 seconds. Indefinitely.