package sk.peefg.javabackend.currency;

import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Peter Seman <seman.peter at gmail.com>
 */
public class TestHkd
{
    @Test
    public void testCurrencyConversionToUsd()
    {
        Hkd hkd = new Hkd();
        Double rate = Hkd.getConversionRate();
        
        Assert.assertEquals(
            Double.valueOf(51.0 * rate),
            hkd.convertToUsd(51.0)
        );
        
    }
}
