package sk.peefg.javabackend.currency;

import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Peter Seman <seman.peter at gmail.com>
 */
public class TestRmb
{
    @Test
    public void testCurrencyConversionToUsd()
    {
        Rmb rmb = new Rmb();
        Double rate = Rmb.getConversionRate();
        
        Assert.assertEquals(
            Double.valueOf(569.469 * rate),
            rmb.convertToUsd(569.469)
        );
        
    }
}
