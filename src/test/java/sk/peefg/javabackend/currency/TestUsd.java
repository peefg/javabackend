package sk.peefg.javabackend.currency;

import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Peter Seman <seman.peter at gmail.com>
 */
public class TestUsd
{
    @Test
    public void testCurrencyConversionToUsd()
    {
        Usd usd = new Usd();
        
        Assert.assertNull(
            usd.convertToUsd(51.0)
        );
        
    }
}
