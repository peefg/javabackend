package sk.peefg.javabackend.currency;

import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Peter Seman <seman.peter at gmail.com>
 */
public class TestAmount
{
    @Test
    public void testGetValue()
    {
        Amounts amounts = new Amounts();
        amounts.setValue(15.5);
        
        Assert.assertEquals(
            Double.valueOf(15.5),
            amounts.getValue()
        );
        
    }
    
    @Test
    public void testGetValueUsd()
    {
        Amounts amounts = new Amounts();
        amounts.setValueUsd(15.7);
        
        Assert.assertEquals(
            Double.valueOf(15.7),
            amounts.getValueUsd()
        );
        
    }
    
}
