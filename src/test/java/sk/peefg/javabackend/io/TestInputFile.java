package sk.peefg.javabackend.io;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;
import sk.peefg.javabackend.currency.CurrencyInterface;
import sk.peefg.javabackend.exceptions.LowAmountException;
import sk.peefg.javabackend.exceptions.MalformedRowException;


/**
 *
 * @author Peter Seman <seman.peter at gmail.com>
 */
public class TestInputFile
{
    
    @Test
    public void testNoFile()
    {
        InputFile inputFile = new InputFile();
        Assert.assertFalse(inputFile.doesTheFileExist());
        
    }
    
    @Test
    public void testFileExist()
    {
        InputFile inputFile = new InputFile("files/file.txt");
        Assert.assertTrue(inputFile.doesTheFileExist());
        
    }
    
    @Test
    public void testFileDoesntExist()
    {
        InputFile inputFile = new InputFile("files/filex.txt");
        Assert.assertFalse(inputFile.doesTheFileExist());
        
    }
    
    @Test
    public void testGetCurrencyClassNotExist()
    {
        InputFile inputFile = new InputFile();
        Assert.assertNull(inputFile.getCurrencyClass("A"));
    }
    
    @Test
    public void testGetCurrencyClassExists()
    {
        InputFile inputFile = new InputFile();
        Assert.assertTrue(
            inputFile.getCurrencyClass("Eur") instanceof CurrencyInterface
        );
    }
    
    @Test
    public void testAddItem()
    {
        boolean     happened    = false;
        String      message     = null;
        InputFile   inputFile   = new InputFile();
        try {
            inputFile.addItem("EUR", -5000.0);
        } catch (LowAmountException | MalformedRowException expectedException) {
             if (expectedException instanceof LowAmountException) {
                happened = true;
                message = expectedException.getMessage();
            }
        }
        
        Assert.assertTrue(happened);
        // too strict, but for simplicity is enough, I guess
        Assert.assertThat(
            message, 
            CoreMatchers.containsString(
                "There is no that much of money, ignoring request"
            )
        );
        
    }
    
    
    
}
