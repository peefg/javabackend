package sk.peefg.javabackend.utils;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;
import sk.peefg.javabackend.exceptions.MalformedRowException;
import sk.peefg.javabackend.exceptions.WrongValueException;

/**
 *
 * @author Peter Seman <seman.peter at gmail.com>
 */
public class TestParseRow
{
    @Test
    public void testExceptionWrongValue()
    {
        
        boolean happened = false;
        try {
            new ParseRow("ABC DEF");
        } catch (MalformedRowException | WrongValueException expectedException) {
            if (expectedException instanceof WrongValueException) {
                happened = true;
            }
        }
        
        Assert.assertTrue(happened);
        
    }
    
    @Test
    public void testExceptionMalformedRow()
    {
        
        boolean happened = false;
        try {
            new ParseRow("ABCDEF");
        } catch (MalformedRowException | WrongValueException expectedException) {
            if (expectedException instanceof MalformedRowException) {
                happened = true;
            }
        }
        
        Assert.assertTrue(happened);
        
    }
    
    @Test
    public void testExceptionMalformedRowAtSpecificRow()
    {
        
        boolean happened = false;
        String message = null;
        
        try {
            new ParseRow("ABC DEF", 3);
        } catch (MalformedRowException | WrongValueException expectedException) {
            if (expectedException instanceof WrongValueException) {
                happened = true;
                message = expectedException.getMessage();
            }
        }
        
        Assert.assertTrue(happened);
        Assert.assertThat(message, CoreMatchers.containsString("at row `3`"));
        
    }
    
    @Test
    public void testGetCurrency()
    {
        
        ParseRow parseRow = null;
        
        try {
            parseRow = new ParseRow("USD 35");
        } catch (MalformedRowException | WrongValueException expectedException) {
        }
        
        Assert.assertEquals("USD", parseRow.getCurrency());
        
    }
    
    @Test
    public void testGetValue()
    {
        
        ParseRow parseRow = null;
        
        try {
            parseRow = new ParseRow("RMB 50");
        } catch (MalformedRowException | WrongValueException expectedException) {
        }
        
        Assert.assertEquals(Double.valueOf(50), parseRow.getValue());
        
    }
    
}
