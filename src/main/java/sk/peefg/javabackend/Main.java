package sk.peefg.javabackend;

import java.util.Scanner;
import java.util.Timer;
import sk.peefg.javabackend.core.AutoConsoleOutput;
import sk.peefg.javabackend.exceptions.LowAmountException;
import sk.peefg.javabackend.exceptions.MalformedRowException;
import sk.peefg.javabackend.exceptions.WrongValueException;
import sk.peefg.javabackend.io.InputFile;
import sk.peefg.javabackend.utils.ParseRow;

/**
 * Main class
 */
public class Main
{

    /**
     * Init method
     * 
     * @param args possible input args
     * - first argument: path to file
     */
    public static void main(String[] args)
    {
     
        InputFile inputFile;
        
        String inputParam = null;
        
        try {
            inputParam = args[0];
        } catch (ArrayIndexOutOfBoundsException e) {
            // nothing;
        }
        
        if (null != inputParam) {
            inputFile = new InputFile(inputParam);
            if (inputFile.doesTheFileExist()) {
                inputFile.parseFile();
            }
        } else {
            inputFile = new InputFile();
        }
        
        Timer timer = new Timer();
        // I could have created some Constants class and put it here...
        timer.scheduleAtFixedRate(
            new AutoConsoleOutput(inputFile),
            60000,
            60000
        );

        String n = null;

        while (!"quit".equals(n)) {
            // Reading from System.in for simplicity
            Scanner reader = new Scanner(System.in);  
            System.out.print("Enter a currency with value: ");
            n = reader.nextLine();

            if (!"quit".equals(n)) {
                ParseRow parseRow;
                try {
                    parseRow = new ParseRow(n);
                    inputFile.addItem(parseRow.getCurrency(), parseRow.getValue());
                } catch (MalformedRowException 
                        | WrongValueException 
                        | LowAmountException ex) {
                    System.out.println("Error: " + ex.getMessage());
                }
            } else {
                inputFile.simpleOutputToConsole();
                System.exit(0);
            }
        }

    }

}
