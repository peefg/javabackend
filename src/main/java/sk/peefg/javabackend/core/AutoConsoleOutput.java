package sk.peefg.javabackend.core;

import java.util.TimerTask;
import sk.peefg.javabackend.io.InputFile;

/**
 * Outputs input to console as TimerTask 
 */
public class AutoConsoleOutput extends TimerTask
{

    private InputFile inputFile;

    /**
     * Constructor sets inputfile
     * 
     * @param inputFile file to input
     */
    public AutoConsoleOutput(InputFile inputFile)
    {
        this.inputFile = inputFile;
    }

    @Override
    /**
     * Run method for timer
     */
    public void run()
    {
        System.out.println("\n------ START OF CURRENCIES AMOUNTS -----");

        this.inputFile.simpleOutputToConsole();

        System.out.println("------ END OF CURRENCIES AMOUNTS -----\n");
    }

}
