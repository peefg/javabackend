package sk.peefg.javabackend.currency;

/**
 * Hkd currency
 */
public class Hkd implements CurrencyInterface
{

    private static final Double USD_CONVERSION_RATE = Double.valueOf(0.388_969);

    public static Double getConversionRate()
    {
        return USD_CONVERSION_RATE;
    }

    public Double convertToUsd(Double input)
    {
        return input * USD_CONVERSION_RATE;
    }

}
