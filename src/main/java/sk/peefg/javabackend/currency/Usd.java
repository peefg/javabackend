package sk.peefg.javabackend.currency;

/**
 * Usd currency
 */
public class Usd implements CurrencyInterface
{

    private static final Double USD_CONVERSION_RATE = Double.valueOf(1.00);

    public static Double getConversionRate()
    {
        return USD_CONVERSION_RATE;
    }

    @Override
    public Double convertToUsd(Double input)
    {
        return null;
    }

}
