package sk.peefg.javabackend.currency;

/**
 * Ensuring transactions of amounts of money for each currency
 */
public class Amounts
{
    private Double value;
    private Double valueUsd;

    public Double getValue()
    {
        return value;
    }

    public void setValue(Double value)
    {
        this.value = value;
    }

    public Double getValueUsd()
    {
        return valueUsd;
    }

    public void setValueUsd(Double valueUsd)
    {
        this.valueUsd = valueUsd;
    }

}
