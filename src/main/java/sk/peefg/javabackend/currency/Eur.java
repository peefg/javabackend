package sk.peefg.javabackend.currency;

/**
 * Eur currency
 */
public class Eur implements CurrencyInterface
{

    private static final Double USD_CONVERSION_RATE = Double.valueOf(1.11);

    public static Double getConversionRate()
    {
        return USD_CONVERSION_RATE;
    }

    @Override
    public Double convertToUsd(Double input)
    {
        return input * USD_CONVERSION_RATE;
    }

}
