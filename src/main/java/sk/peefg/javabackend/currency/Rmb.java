package sk.peefg.javabackend.currency;

/**
 * Rmb currency
 */
public class Rmb implements CurrencyInterface
{

    private static final Double USD_CONVERSION_RATE = Double.valueOf(1.58_89);

    public static Double getConversionRate()
    {
        return USD_CONVERSION_RATE;
    }

    public Double convertToUsd(Double input)
    {
        return input * USD_CONVERSION_RATE;
    }



}
