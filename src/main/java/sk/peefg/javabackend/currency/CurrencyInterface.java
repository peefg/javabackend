package sk.peefg.javabackend.currency;

/**
 * Currency Interface
 */
public interface CurrencyInterface
{

    public abstract Double convertToUsd(Double input);

}
