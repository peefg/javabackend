package sk.peefg.javabackend.utils;

import sk.peefg.javabackend.exceptions.MalformedRowException;
import sk.peefg.javabackend.exceptions.WrongValueException;

/**
 * Handling parsing row
 */
public class ParseRow
{

    private String  row         = null;
    private String  currency    = null;
    private Double  value       = 0.0;
    private int     lineNumber  = 0;

    /**
     * Constructor used for parsing input file
     * 
     * @param   row             row to parse
     * @param   lineNumber      line number (for debugging/logging purposes)
     * 
     * @throws  MalformedRowException   if row doesn't have two parts
     * @throws  WrongValueException     if amount is not a number
     */
    public ParseRow(String row, int lineNumber)
            throws MalformedRowException, WrongValueException
    {
        this.row = row;
        this.lineNumber = lineNumber;
        this.checkRowStructure();
    }

    /**
     * Constructor used for parsing manual input
     * 
     * @param   row             row to parse
     * 
     * @throws  MalformedRowException   if row doesn't have two parts
     * @throws  WrongValueException     if amount is not a number
     */
    public ParseRow(String row)
            throws MalformedRowException, WrongValueException
    {
        this.row = row;
        this.checkRowStructure();
    }

    /**
     * Checking row structure
     * 
     * @throws MalformedRowException    if row doesn't have two parts
     * @throws WrongValueException      if amount is not a number
     */
    private void checkRowStructure()
            throws MalformedRowException,
            WrongValueException
    {

        String[] items = this.row.split(" ");

        if (items.length != 2) {
            throw new MalformedRowException("Malformed structure");
        }

        this.currency = items[0];

        try {
            this.value = Double.valueOf(items[1]);
        } catch (NumberFormatException e) {
            String error = "";
            if (0 != this.lineNumber) {
                error = "Error parsing value: `" + items[1] 
                        + "` at row `" + this.lineNumber + "`";
            } else {
                error = "Error parsing value: `" + items[1] + "`";
            }
            // I wanted to rethrow my exception with my error message...
            throw new WrongValueException(error);
        }

    }

    /**
     * Gets currency name
     * 
     * @return currency name
     */
    public String getCurrency()
    {
        return this.currency;
    }

    /**
     * Gets value of currency
     * 
     * @return amoutn of money held for currency
     */
    public Double getValue()
    {
        return this.value;
    }

}
