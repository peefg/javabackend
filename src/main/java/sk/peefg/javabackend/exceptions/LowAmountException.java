package sk.peefg.javabackend.exceptions;

/**
 * LowAmountException
 */
public class LowAmountException extends Exception
{
    
    public LowAmountException(String message)
    {
        super(message);
    }
    
}
