package sk.peefg.javabackend.exceptions;

/**
 * MalformedRowException
 */
public class MalformedRowException extends Exception
{
    
    public MalformedRowException(String message)
    {
        super(message);
    }
    
}
