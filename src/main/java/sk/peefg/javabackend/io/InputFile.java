package sk.peefg.javabackend.io;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import sk.peefg.javabackend.currency.Amounts;
import sk.peefg.javabackend.exceptions.LowAmountException;
import sk.peefg.javabackend.exceptions.MalformedRowException;
import sk.peefg.javabackend.exceptions.WrongValueException;
import sk.peefg.javabackend.utils.ParseRow;
import sk.peefg.javabackend.currency.CurrencyInterface;

/**
 * Class which handles parsing input file or adding rows
 */
public class InputFile
{

    private       File                 file           = null;
    private final Map<String, Amounts> itemsToOutput  = new HashMap();

    /**
     * Constructor sets file from path
     * 
     * @param path to file
     */
    public InputFile(String path)
    {
        this.file = new File(path);
    }
    
    /**
     * Empty constructor, where file is not provided
     */
    public InputFile()
    {
        // empty constructor, do not prepare anything
    }

    /**
     * Adds item to array..
     *
     * @param currency currency name to add
     * @param value    amount of money
     * 
     * @throws MalformedRowException    if row doesn't have two parts
     * @throws LowAmountException       if amount to decrease it too low
     */
    public void addItem(
            String currency, 
            Double value
    )  throws   LowAmountException, 
                MalformedRowException
    {
        processValues(currency, value, true);
        
    }

    /**
     * Checks whether file exists
     * and outputs message if doesn't
     * 
     * @return true|false
     */
    public boolean doesTheFileExist()
    {
        
        if (null == this.file) {
            System.out.println("No input file provided");
            return false;
        }

        boolean doesExist = this.file.exists();
        if (false == doesExist) {
            System.out.println("Input file doesn't exist");
        }
        return doesExist;
        
    }

    /**
     * Parses input file and adds what can be added
     */
    public void parseFile()
    {
        try {

            AtomicInteger lineNumber = new AtomicInteger(1);

            Files.lines(this.file.toPath(), StandardCharsets.UTF_8).forEach(
                line -> {

                    try {
                        // checking for rows only in file..
                        ParseRow row = new ParseRow(
                            line, 
                            lineNumber.getAndIncrement()
                        );
                        
                        processValues(row.getCurrency(), row.getValue(), false);
                        
                    } catch (MalformedRowException 
                            | WrongValueException 
                            | LowAmountException ex) {
                        System.out.println("Error: " + ex.getMessage());
                    }

                }
            );
        } catch (IOException ex) {
            System.out.println("Error: " + ex.getMessage());
        }

    }

    /**
     * Very simple output to system console
     */
    public void simpleOutputToConsole()
    {
        this.itemsToOutput.forEach((k, v) -> {

            String toOut;
            if (null == v.getValueUsd()) {
                toOut = k + " " + v.getValue();
            } else {
                toOut = k + " " + v.getValue() + " (USD: " + v.getValueUsd() + ")";
            }

            System.out.println(toOut);
        });
    }

    /**
     * Gets currency class if exists
     * 
     * @param currency  CurrencyInterface - case insensitive
     * 
     * @return Class instance|null
     */
    public CurrencyInterface getCurrencyClass(String currency)
    {

        CurrencyInterface clsIns = null;

        try {

            String classCurrency = currency.substring(0, 1).toUpperCase()
                + currency.substring(1).toLowerCase();
            Class cls = 
                Class.forName("sk.peefg.javabackend.currency." + classCurrency);
            clsIns = (CurrencyInterface) cls.newInstance();
        } catch (InstantiationException
                | IllegalAccessException
                | ClassNotFoundException ex) {
            // we don't care where it get killed, important is the action
            // would have gone to some logger ->> ex.getMessage()...
            return null;
        }

        return clsIns;
    }

    /**
     * Gets right word for updating value
     * 
     * @param value input amount
     * 
     * @return specific string
     */
    private String updateValueSign(Double value)
    {
        return value < 0 ? "descreased" : "increased";
    }
    
    /**
     * Modifies values in out Map, doing all the logic
     * 
     * @param currency      currency to add
     * @param value         amount of money to add/remove
     * @param showResult    whether we'd like to see result in console
     * 
     * @throws MalformedRowException    if row doesn't have two parts
     * @throws LowAmountException       if amount to decrease it too low
     */
    private void processValues(
            String currency, 
            Double value,
            boolean showResult
    ) throws 
            MalformedRowException, 
            LowAmountException
    {
        CurrencyInterface currencyClass = this.getCurrencyClass(currency);

        if (null == currencyClass) {
            throw new MalformedRowException(
                "Error adding currency `" + currency + "`, "
                + "has not been implemneted yet");
        }

        if (this.itemsToOutput.containsKey(currency)) {

            Double newDouble
                    = value + this.itemsToOutput
                            .get(currency)
                            .getValue();
            Amounts am = new Amounts();
            am.setValue(newDouble);
            if (newDouble < 0) {
                throw new LowAmountException(
                    "There is no that much of money, ignoring request"
                );
            }
            am.setValueUsd(
                    this.getCurrencyClass(currency)
                            .convertToUsd(newDouble)
            );
            this.itemsToOutput.replace(currency, am);

        } else {
            Amounts am = new Amounts();
            am.setValue(value);
            if (value < 0) {
                throw new LowAmountException(
                    "There is no that much of money, ignoring request"
                );
            }
            am.setValueUsd(
                    this.getCurrencyClass(currency)
                            .convertToUsd(value)
            );
            this.itemsToOutput.put(currency, am);
        }
        
        if (true == showResult) {
            
            System.out.println(
                "Currency `" + currency + "` "
                + this.updateValueSign(value) + " by value `" + value + "`"
            );
            
        }
    }


}
